<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <title>Register V3 | Remark Admin Template</title>
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="global/css/bootstrap.min.css">
    <link rel="stylesheet" href="global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="assets/css/site.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="global/vendor/waves/waves.css">
    <link rel="stylesheet" href="assets/examples/css/pages/register-v3.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <!--[if lt IE 9]>
    <script src="global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="global/vendor/media-match/media.match.min.js"></script>
    <script src="global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
         var pass=0;
         var errorname =0;
         var  errorname=11;
         var  errors_email=32;
         var  pass=33;

    </script>
</head>
<body class="animsition page-register-v3 layout-full">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Page -->
<div class="page vertical-align text-xs-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle">
        <div class="panel">
            <div class="panel-body">
                <div class="brand">
                    <img class="brand-img" src="assets//images/logo-blue.png" alt="...">
                    <h2 class="brand-text font-size-18">Team_Var</h2>
                    <div id="errors" ><p class="formTitle">Регистрирация</p></div>
                </div>
                <form id="secondForm">

                    <div id="errors_name" class="form-group form-material floating" data-plugin="formMaterial">
                        <input id="name" type="text" class="form-control" name="name1"  />
                        <label class="floating-label">Имя и Фамилия</label>
                    </div>
                    <div id="errors_nick"  class="form-group form-material floating" data-plugin="formMaterial">
                        <input  id="nick" type="text" class="form-control" name="nick1"   />
                        <label class="floating-label">Игровой ник</label>
                    </div>
                    <div id="errors_email" class="form-group form-material floating" data-plugin="formMaterial">
                        <input id="email" type="email" class="form-control" name="email1"  />
                        <label class="floating-label">Email</label>
                    </div>
                    <div  id="errors_password" class="form-group form-material floating" data-plugin="formMaterial">
                        <input  id="password" type="password" class="form-control" name="password1"  />
                        <label  class="floating-label">Пароль  минимум 6 символов</label>
                    </div>
                    <div id="errors_PasswordCheck" class="form-group form-material floating" data-plugin="formMaterial">
                        <input  id="PasswordCheck" type="password" class="form-control"  name="PasswordCheck1"  />
                        <label class="floating-label">Повторите пароль</label>
                    </div>
                    <button type="submit" id="button" class="btn btn-primary btn-block btn-lg m-t-40"   >Регистрация</button>
                </form>
                <p>Есть учетка тогда войди <a href="index.php">Авторизация</a></p>
            </div>
        </div>
        <footer class="page-copyright page-copyright-inverse">
            <p>Тестовая версия © 2017.</p>
        </footer>
    </div>
</div>
<!-- End Page -->
<!-- Core  -->
<script src="global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
<script src="global/vendor/jquery/jquery.js"></script>
<script src="global/vendor/tether/tether.js"></script>
<script src="global/vendor/bootstrap/bootstrap.js"></script>
<script src="global/vendor/animsition/animsition.js"></script>
<script src="global/vendor/mousewheel/jquery.mousewheel.js"></script>
<script src="global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
<script src="global/vendor/asscrollable/jquery-asScrollable.js"></script>
<script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
<script src="global/vendor/waves/waves.js"></script>
<!-- Plugins -->
<script src="global/vendor/switchery/switchery.min.js"></script>
<script src="global/vendor/intro-js/intro.js"></script>
<script src="global/vendor/screenfull/screenfull.js"></script>
<script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
<script src="global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<!-- Scripts -->
<script src="global/js/State.js"></script>
<script src="global/js/Component.js"></script>
<script src="global/js/Plugin.js"></script>
<script src="global/js/Base.js"></script>
<script src="global/js/Config.js"></script>
<script src="assets/js/Section/Menubar.js"></script>
<script src="assets/js/Section/GridMenu.js"></script>
<script src="assets/js/Section/Sidebar.js"></script>
<script src="assets/js/Section/PageAside.js"></script>
<script src="assets/js/Plugin/menu.js"></script>
<script src="global/js/config/colors.js"></script>
<script src="assets/js/config/tour.js"></script>
<script>
    Config.set('assets', 'assets');
</script>
<!-- Page -->
<script src="assets/js/Site.js"></script>
<script src="global/js/Plugin/asscrollable.js"></script>
<script src="global/js/Plugin/slidepanel.js"></script>
<script src="global/js/Plugin/switchery.js"></script>
<script src="global/js/Plugin/jquery-placeholder.js"></script>
<script src="global/js/Plugin/material.js"></script>
<script>
    (function(document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);
    var password,PasswordCheck,name,nick,email;
    $(document).on('click','#button',function(e){
        e.preventDefault();
        //var id = $(this).data('id') || 0;
        name=document.getElementById("name").value;
        nick=document.getElementById("nick").value;
        email=document.getElementById("email").value;
        password=document.getElementById("password").value;
        PasswordCheck=document.getElementById("PasswordCheck").value;
        $("#errors").load("public/scr/reg.php",{password:password,PasswordCheck:PasswordCheck,email:email, nick:nick,name:name});

        // работа с именем
        if(errorname==1){
            console.log(errorname);
            $('#name').val('');
            $('#errors_name').removeClass('form-group form-material floating').addClass(' form-group form-material has-danger');
        }else if (errorname==2){
            $('#errors_name').removeClass('form-group form-material has-danger').addClass(' form-group form-material floating');
        };
        if(errors_nick==1){
            console.log(errors_nick);
            $('#errors_nick').val('');
            $('#errors_nick').removeClass('form-group form-material floating').addClass(' form-group form-material has-danger');
        }else if (errors_nick==2){
            $('#errors_name').removeClass('form-group form-material has-danger').addClass(' form-group form-material floating');
        };
        // почта
        if(errors_email==1){
            $('#email').val('');
            $('#errors_email').removeClass('form-group form-material floating').addClass(' form-group form-material has-danger');
        }else if (errors_email==2){
            $('#errors_email').removeClass('form-group form-material has-danger').addClass(' form-group form-material floating');
        };
        if(pass==2){
            console.log('тест');
            $('#password').val('');
            $('#PasswordCheck').val('');
        }else if(pass==3){
            $('#password').val('');
            $('#PasswordCheck').val('');
            $('#errors_password').removeClass('form-group form-material floating').addClass(' form-group form-material has-danger');
        }else if (pass==4){
            $('#password').val('');
            $('#PasswordCheck').val('');
            $('#errors_password').removeClass('form-group form-material has-danger').addClass(' form-group form-material floating');
        };
    });
</script>
</body>
</html>