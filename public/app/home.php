<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <title>Список всех команд</title>
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="global/css/bootstrap.min.css">
    <link rel="stylesheet" href="global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="assets/css/site.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="global/vendor/waves/waves.css">
    <link rel="stylesheet" href="global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
    <link rel="stylesheet" href="global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
    <link rel="stylesheet" href="global/vendor/datatables-responsive/dataTables.responsive.css">
    <link rel="stylesheet" href="assets/examples/css/tables/datatable.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="global/fonts/font-awesome/font-awesome.css">
    <link rel="stylesheet" href="global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <!--[if lt IE 9]>
    <script src="global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="global/vendor/media-match/media.match.min.js"></script>
    <script src="global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
    </script>
</head>
<body class="animsition">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
            <i class="icon md-more" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <img class="navbar-brand-logo" src="assets/images/logo.png" title="Remark">
            <span class="navbar-brand-text hidden-xs-down"> Remark</span>
        </div>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search"
                data-toggle="collapse">
            <span class="sr-only">Toggle Search</span>
            <i class="icon md-search" aria-hidden="true"></i>
        </button>
    </div>
    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar -->
            <ul class="nav navbar-toolbar">
                <li class="nav-item hidden-float" id="toggleMenubar">
                    <a class="nav-link" data-toggle="menubar" href="#" role="button">
                        <i class="icon hamburger hamburger-arrow-left">
                            <span class="sr-only">Toggle menubar</span>
                            <span class="hamburger-bar"></span>
                        </i>
                    </a>
                </li>
                <li class="nav-item hidden-sm-down" id="toggleFullscreen">
                    <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                        <span class="sr-only">Toggle fullscreen</span>
                    </a>
                </li>
                <li class="nav-item hidden-float">
                    <a class="nav-link icon md-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
                       role="button">
                        <span class="sr-only">Toggle Search</span>
                    </a>
                </li>
                <li class="nav-item dropdown dropdown-fw dropdown-mega">

                </li>
            </ul>
            <!-- End Navbar Toolbar -->
            <!-- Navbar Toolbar Right -->
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                <? require 'public/module/nav.php'?>
            </ul>
            <!-- End Navbar Toolbar Right -->
        </div>
        <!-- End Navbar Collapse -->
        <!-- Site Navbar Seach -->
        <div class="collapse navbar-search-overlap" id="site-navbar-search">
            <form role="search">
                <div class="form-group">
                    <div class="input-search">
                        <i class="input-search-icon md-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="site-search" placeholder="Search...">
                        <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
                                data-toggle="collapse" aria-label="Close"></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- End Site Navbar Seach -->
    </div>
</nav>
<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <? require 'public/module/menu.php'?>
            </div>
        </div>
    </div>
    <div class="site-menubar-footer">
        <? require 'public/module/button.php'?>
    </div>
</div>
<div class="site-gridmenu">
    <div>
        <div>
            <ul>
                <li>
                    <a href="../apps/mailbox/mailbox.html">
                        <i class="icon md-email"></i>
                        <span>Mailbox</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/calendar/calendar.html">
                        <i class="icon md-calendar"></i>
                        <span>Calendar</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/contacts/contacts.html">
                        <i class="icon md-account"></i>
                        <span>Contacts</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/media/overview.html">
                        <i class="icon md-videocam"></i>
                        <span>Media</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/documents/categories.html">
                        <i class="icon md-receipt"></i>
                        <span>Documents</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/projects/projects.html">
                        <i class="icon md-image"></i>
                        <span>Project</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/forum/forum.html">
                        <i class="icon md-comments"></i>
                        <span>Forum</span>
                    </a>
                </li>
                <li>
                    <a href="../index.html">
                        <i class="icon md-view-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Page -->
<div class="page">
    <div class="page-header">
        <h1 class="page-title">Список команд которым доверяют нащи игроки</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Главная</a></li>
            <li class="breadcrumb-item"><a href="index.php?app=list_team&menu=1">Список команд</a></li>
            <li class="breadcrumb-item active">Белый список</li>
        </ol>
        <div class="page-header-actions">
            <a type="button" href="index.php?app=team_add" class="btn btn-floating btn-primary btn-sm waves-effect"><i class="icon md-plus" aria-hidden="true"></i></a>
        </div>
    </div>
    <div class="page-content">
        <div class="panel">
            <header class="panel-heading">
                <h3 class="panel-title">
                    Список
                </h3>
            </header>
            <div class="panel-body">
                <table class="table table-hover dataTable table-striped w-full"  id="example">
                    <thead>
                    <tr>
                        <th>Страна</th>
                        <th class="input-filter">Название</th>
                        <th>Побед</th>
                        <th>Поражений</th>
                        <th class="select-filter">Правила</th>

                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Страна</th>
                        <th>Название</th>
                        <th>Побед</th>
                        <th>Поражений</th>
                        <th>Правила</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?
                    $qr_td = mysql_query("SELECT * FROM `$db_name`.`list_teams`") or die(mysql_error());
                    while ($data_team = mysql_fetch_array($qr_td)) {
                        $type=array(1=>'Все','Форум','Арена');
                        $val_vin=rand(1,200);
                        $val_lose=rand(1,200);
                        $glas = ["a","e","i","y","o","u"];
                        $soglas = ["b","c","d","f","g","h","j","k","l","m","n","p","q","r","s","t","v","x","w","z"];

                        $wordlen = 5;

                        for ($i=0; $i <$wordlen/2 ; $i++) {
                            $ng = rand(0, count($glas) - 1);
                            $nsg = rand(0, count($soglas) - 1);
                            $newWord .= $glas[$ng].$soglas[$nsg];
                        }
                        $mass_flag=array('ru','az','ua','by');
                        printf('<tr>
                                                <td><span class="flag-icon flag-icon-%s"></span></td>
                                                <td><a href="index.php?app=team_prof&id=%s&menu=1">%s</a></td>
                                                <td>%s</td>
                                                <td>%s</td>
                                                <td>%s</td>
                                          </tr>',$mass_flag[$data_team['flag']],$data_team['id'],$data_team['name'],$val_vin,$val_lose,$type[$data_team['type']]);
                        unset($newWord);
                    };
                    ?>
                    </tbody>
                </table>

            </div>
        </div>
        <!-- End Panel FixedHeader -->
    </div>
</div>
<!-- End Page -->
<!-- Footer -->
<footer class="site-footer">
    <? require 'public/module/footer.php'?>
</footer>
<!-- Core  -->

<script src="global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
<script src="global/vendor/jquery/jquery.js"></script>
<script src="global/vendor/tether/tether.js"></script>
<script src="global/vendor/bootstrap/bootstrap.js"></script>
<script src="global/vendor/animsition/animsition.js"></script>
<script src="global/vendor/mousewheel/jquery.mousewheel.js"></script>
<script src="global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
<script src="global/vendor/asscrollable/jquery-asScrollable.js"></script>
<script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
<script src="global/vendor/waves/waves.js"></script>
<!-- Plugins -->

<script src="global/vendor/switchery/switchery.min.js"></script>
<script src="global/vendor/intro-js/intro.js"></script>
<script src="global/vendor/screenfull/screenfull.js"></script>
<script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
<script src="global/vendor/datatables/jquery.dataTables.js"></script>
<script src="global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
<script src="global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>
<script src="global/vendor/asrange/jquery-asRange.min.js"></script>
<script src="global/vendor/bootbox/bootbox.js"></script>
<!-- Scripts -->
<script src="global/js/State.js"></script>
<script src="global/js/Component.js"></script>
<script src="global/js/Plugin.js"></script>
<script src="global/js/Base.js"></script>
<script src="global/js/Config.js"></script>
<script src="assets/js/Section/Menubar.js"></script>
<script src="assets/js/Section/GridMenu.js"></script>
<script src="assets/js/Section/Sidebar.js"></script>
<script src="assets/js/Section/PageAside.js"></script>
<script src="assets/js/Plugin/menu.js"></script>
<script src="global/js/config/colors.js"></script>
<script src="assets/js/config/tour.js"></script>

<!-- Page -->
<script src="assets/js/Site.js"></script>
<script src="global/js/Plugin/asscrollable.js"></script>
<script src="global/js/Plugin/slidepanel.js"></script>
<script src="global/js/Plugin/switchery.js"></script>

<script>

    $(document).ready(function() {
        // Setup - add a text input to each footer cell
        $('.input-filter').each( function () {
            var title = $(this).text();
            $(this).html( '<input class="form-control input-sm" type="text" placeholder="'+title+'" />' );
        } );

        // DataTable


        var table = $('#example').DataTable( {
            initComplete: function () {
                this.api().columns('.select-filter').every( function () {
                    var column = this;
                    var select = $('<select  class="form-control input-sm"><option value="" ></option></select>')
                        .appendTo( $(column.header()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            },
            "columnDefs": [
                { "width": "2%", "targets": 0 },
                { "width": "68%", "targets": 1 },
                { "width": "10%", "targets": 2 },
                { "width": "10%", "targets": 3 },
                { "width": "10%", "targets": 4 }
            ],
            buttons: true,
            ordering: false,
            language: {
                search: "Поиск:",
                decimal:        "",
                emptyTable:     "Нет данных, доступных в таблице",
                info:           "Показывать  _START_ из _END_ всего _TOTAL_ Записей",
                infoEmpty:      "Показывать  0 из 0 всего 0 Записей",
                infoFiltered:   "(Отфильтрован из _MAX_ total Команд)",
                infoPostFix:    "",
                thousands:      ",",
                lengthMenu:     "Показывать _MENU_ Команд",
                loadingRecords: "Загрузка...",
                processing:     "Поцесс...",
                zeroRecords:    "No matching records found",
                paginate: {
                    first:      "Первая",
                    last:       "Последняя",
                    next:       "Следующая",
                    previous:   "Предыдущая"
                },
                aria: {
                    "sortAscending":  ": Активировать для сортировки столбцов по возрастанию",
                    "sortDescending": ": Активировать сортировку по убыванию"
                }
            }
        });

        // Apply the search
        table.columns('.input-filter').every( function () {
            var that = this;

            $( 'input', this.header() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );
    } );
</script>

<script src="assets/examples/js/uikit/icon.js"></script>
</body>
</html>