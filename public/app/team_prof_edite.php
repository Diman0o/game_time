
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en" ng-app="courseListApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <script src="global/angular.min.js"></script>
    <title>Команда  | <?=team_profile($_GET['id'],1)?></title>
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="global/css/bootstrap.min.css">
    <link rel="stylesheet" href="global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="assets/css/site.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="global/vendor/waves/waves.css">
    <link rel="stylesheet" href="assets/examples/css/pages/profile.css">
    <link rel="stylesheet" href="global/vendor/toastr/toastr.css">
    <link rel="stylesheet" href="assets/examples/css/advanced/toastr.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <!--[if lt IE 9]>
    <script src="global/angular.min.js"></script>
    <script src="global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="global/vendor/media-match/media.match.min.js"></script>
    <script src="global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="global/vendor/breakpoints/breakpoints.js"></script>
    <script>


        var users = {
            courses: <?=team_members($_GET['id'])?>
        };
        var matches = {
            courses: <?=team_matches($_GET['id'])?>
        };

        // Модуль

        var courseListApp = angular.module("courseListApp", []);

        // Контроллер

        courseListApp.controller("CourseListUsers", function ($scope) {

            $scope.data = users;

            $scope.addNewCourse = function () {

                $scope.data.courses.push({
                    name: $scope.courseName,
                    passed: false
                });

                $scope.courseName = "";
            }

            $scope.showText = function (passed) {
                //$scope.passed = ;
                return passed ?  's' : 'ss';
            }
        });
        courseListApp.controller("CourseListMatches", function ($scope) {

            $scope.data = matches;

            $scope.addNewCourse = function () {

                $scope.data.courses.push({
                    name: $scope.courseName,
                    passed: false
                });

                $scope.courseName = "";
            }

            $scope.showText = function (passed) {
                //$scope.passed = ;
                return passed ?  's' : 'ss';
            }
        });
        var modal_war=1;
        var modal_add=1;
        Breakpoints();
    </script>
</head>
<body class="animsition page-profile" >
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
            <i class="icon md-more" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <img class="navbar-brand-logo" src="assets/images/logo.png" title="Remark">
            <span class="navbar-brand-text hidden-xs-down"> Remark</span>
        </div>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search"
                data-toggle="collapse">
            <span class="sr-only">Toggle Search</span>
            <i class="icon md-search" aria-hidden="true"></i>
        </button>
    </div>
    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar -->
            <ul class="nav navbar-toolbar">
                <li class="nav-item hidden-float" id="toggleMenubar">
                    <a class="nav-link" data-toggle="menubar" href="#" role="button">
                        <i class="icon hamburger hamburger-arrow-left">
                            <span class="sr-only">Toggle menubar</span>
                            <span class="hamburger-bar"></span>
                        </i>
                    </a>
                </li>
                <li class="nav-item hidden-sm-down" id="toggleFullscreen">
                    <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                        <span class="sr-only">Toggle fullscreen</span>
                    </a>
                </li>
                <li class="nav-item hidden-float">
                    <a class="nav-link icon md-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
                       role="button">
                        <span class="sr-only">Toggle Search</span>
                    </a>
                </li>
                <li class="nav-item dropdown dropdown-fw dropdown-mega">

                </li>
            </ul>
            <!-- End Navbar Toolbar -->
            <!-- Navbar Toolbar Right -->
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                <? require 'public/module/nav.php'?>
            </ul>
            <!-- End Navbar Toolbar Right -->
        </div>
        <!-- End Navbar Collapse -->
        <!-- Site Navbar Seach -->
        <div class="collapse navbar-search-overlap" id="site-navbar-search">
            <form role="search">
                <div class="form-group">
                    <div class="input-search">
                        <i class="input-search-icon md-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="site-search" placeholder="Search...">
                        <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
                                data-toggle="collapse" aria-label="Close"></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- End Site Navbar Seach -->
    </div>
</nav>
<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <? require 'public/module/menu.php'?>
            </div>
        </div>
    </div>
    <div class="site-menubar-footer">
        <? require 'public/module/button.php'?>
    </div>
</div>
<div class="site-gridmenu">
    <div>
        <div>
            <ul>
                <li>
                    <a href="../apps/mailbox/mailbox.html">
                        <i class="icon md-email"></i>
                        <span>Mailbox</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/calendar/calendar.html">
                        <i class="icon md-calendar"></i>
                        <span>Calendar</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/contacts/contacts.html">
                        <i class="icon md-account"></i>
                        <span>Contacts</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/media/overview.html">
                        <i class="icon md-videocam"></i>
                        <span>Media</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/documents/categories.html">
                        <i class="icon md-receipt"></i>
                        <span>Documents</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/projects/projects.html">
                        <i class="icon md-image"></i>
                        <span>Project</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/forum/forum.html">
                        <i class="icon md-comments"></i>
                        <span>Forum</span>
                    </a>
                </li>
                <li>
                    <a href="../index.html">
                        <i class="icon md-view-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Page -->
<div class="page">
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-xs-12 col-lg-3">
                <!-- Page Widget -->
                <div class="card card-shadow text-xs-center">
                    <div class="card-block">
                        <a class="avatar avatar-lg" href="javascript:void(0)">
                            <img src="<?=team_profile($_GET['id'],2)?>" alt="<?=team_profile($_GET['id'],1)?>">
                        </a>
                        <h4 class="profile-user"> <span class="flag-icon flag-icon-<?=team_profile($_GET['id'],4)?>"></span> <?=team_profile($_GET['id'],1)?></h4>
                        <div class="example">
                            <div class="rating" data-score="3" data-plugin="rating" style="cursor: pointer;"><i data-alt="1" class="icon md-star orange-600" title="bad"></i>&nbsp;<i data-alt="2" class="icon md-star orange-600" title="poor"></i>&nbsp;<i data-alt="3" class="icon md-star" title="regular"></i>&nbsp;<i data-alt="4" class="icon md-star" title="good"></i>&nbsp;<i data-alt="5" class="icon md-star" title="gorgeous"></i><input name="score" type="hidden" value="2"></div>
                        </div>

                        <p class="profile-job">Статус: Готовы </p>
                        <div class="btn-group btn-group-vertical" aria-label="Small button group" role="group">
                            <!--<button type="button" class="btn btn-primary waves-effect">Подписатся</button>-->
                            <?
                            if(button_change_team($_GET['id'])!=0){
                                printf('<a href="index.php?app=team_prof_edite&id=%s"  type="button" class="btn btn-primary waves-effect">
                                            <i class="icon md-time" aria-hidden="true"></i><br>
                                            <span class="text-uppercase hidden-sm-down">Управление</span>
                                           </a>',$_GET['id']);};
                            if(button_war($_GET['id'])==1){
                                ?>
                                <button data-target="#team_war" data-toggle="modal" type="button" class="btn btn-danger waves-effect">
                                    <i class="icon md-time" aria-hidden="true"></i><br>
                                    <span class="text-uppercase hidden-sm-down">Вызов</span></button>
                                <!--modal--->
                                <div class="modal fade modal-fade-in-scale-up" id="team_war" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title">Вызов команды на противостояние</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="errors">

                                                </div>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="example-wrap m-sm-0">
                                                        <h4 class="example-title">Выберите павила</h4>
                                                        <div class="form-group">
                                                            <select id="param" class="form-control">
                                                                <option value="1">Арена</option>
                                                                <option value="2">SLTV Only</option>
                                                                <option value="3">Дредноут</option>
                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="example-wrap m-sm-0">
                                                        <h4 class="example-title">Выберите Дату</h4>
                                                        <div class="form-group">
                                                            <input id="date" type="text" class="form-control" value="<?=date('Y-m-d');?>">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="example-wrap m-sm-0">
                                                        <h4 class="example-title">Справка</h4>
                                                        <p>
                                                            Арена* это правила  подразумевающие сезон текуший <br>
                                                            SlTV* правила в которых нету дуалов и с4 и карты Убежище<br>
                                                            Дредноут* правила в которых можно все <br>
                                                            Желаем приятной и честной игры всем участникам
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-pure waves-effect" data-dismiss="modal">Отмена</button>
                                                <button id="button"    type="button" class="btn btn-primary waves-effect">Отправить заявку</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--modal--->
                                <?
                            };
                            if( status_in_team($_GET['id'])==0){ ?>
                                <button data-target="#team_add" data-toggle="modal"   type="button" class="btn btn-info waves-effect"><i class="icon md-time" aria-hidden="true"></i><br>
                                    <span class="text-uppercase hidden-sm-down">Вступить в команду</span></button>
                                <!--modal--->
                                <div class="modal fade modal-fade-in-scale-up" id="team_add" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title">Вступление в состав команды</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row" >
                                                    <div class="col-xs-12 col-md-12" id="joins"></div>
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="example-wrap m-sm-0">
                                                            <h4 class="example-title">Пароль вступления</h4>
                                                            <input id="text" type="text" class="form-control" placeholder="Введите код на Вступление или аренду">
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="example-wrap m-sm-0">
                                                            <h4 class="example-title">Справка</h4>
                                                            <p>
                                                                тестовый режим работы сайта в случаии не работы сообщате в личные сообщения   <a  target="_blank" href="https://vk.com/dimanoo">Админестрации</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-pure waves-effect" data-dismiss="modal">Отмена</button>
                                                <button id="join"    type="button" class="btn btn-primary waves-effect">Отправить заявку</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--modal---><?}elseif(status_in_team($_GET['id'])==1){;?>

                                    <button id="out_team"  type="button" class="btn btn-danger waves-effect"><i class="icon md-time" aria-hidden="true"></i><br>
                                        <span class="text-uppercase hidden-sm-down"> Покинуть команду</span></button>

                            <?};?>
                            <a class="btn btn-primary"  href="index.php?app=team_prof&id=<?=$_GET['id']?>" role="button">Стараница команды</a>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row no-space">
                            <? $team_col_game = new Teams_col_var();?>
                            <div class="col-xs-4">
                                <strong class="profile-stat-count"><? echo $team_col_game->team_wins($_GET['id']);?></strong>
                                <span>Побед</span>
                            </div>
                            <div class="col-xs-4">
                                <strong class="profile-stat-count"><? echo $team_col_game->team_draw($_GET['id']);?></strong>
                                <span>Ничьи</span>
                            </div>
                            <div class="col-xs-4">
                                <strong class="profile-stat-count"><? echo $team_col_game->team_loss($_GET['id']);?></strong>
                                <span>Поражений</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Page Widget -->
            </div>
            <div class="col-xs-12 col-lg-9">
                <!-- Panel -->
                <div class="page-content container-fluid">
                    <div class="row">
                        <div class="col-xxl-6 col-lg-6 col-xs-12">
                            <!-- Example Heading With Desc Bottom -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">О команде</h3>
                                </div>
                                <div id="out"></div>
                                <div class="panel-body">
                                    Название:<br> <input type="text" class="form-control" id="inputPlaceholder"  value="<?=team_profile($_GET['id'],1)?>" disabled >  <br>
                                    Код вступления :
                                    <br>
                                    <div class="input-group">
                                        <input  id="code_gen_value" type="text" class="form-control" name="" value="<?=team_profile($_GET['id'],8)?>">
                                        <span class="input-group-btn">
                                          <button id="code_gen" type="submit" class="btn btn-primary waves-effect"><i class="icon md-refresh" aria-hidden="true"></i></button>
                                        </span>
                                    </div>
                                    <br>
                                    Страна :
                                    <br>
                                    <div class="form-group">
                                        <select id="contru" class="form-control">
                                            <option value="0"> <span class="flag-icon flag-icon-<?=team_profile($_GET['id'],4)?>"></span> Россия</option>
                                            <option value="1"> <span class="flag-icon flag-icon-<?=team_profile($_GET['id'],4)?>"></span> Азербайджан</option>
                                            <option value="2"> <span class="flag-icon flag-icon-<?=team_profile($_GET['id'],4)?>"></span> Украина</option>
                                            <option value="3"> <span class="flag-icon flag-icon-<?=team_profile($_GET['id'],4)?>"></span> Белоруссия</option>
                                        </select>
                                    </div>
                                    Правила предпочитаемые:
                                    <br>
                                    <div class="form-group">
                                        <select id="rules" class="form-control">
                                            <option value="0"> Все</option>
                                            <option value="1"> Арена</option>
                                            <option value="1"> Дредноут</option>
                                        </select>
                                    </div>
                                    <br>
                                    О команде: <br>
                                    <textarea class="form-control" id="textareaDefault" rows="3"><?=team_profile($_GET['id'],5)?></textarea>
                                    <br>

                                    <a class="btn btn-primary" id="button_about" data-plugin="toastr" data-message="Все изменения сохранены"
                                       data-container-id="toast-top-right" data-show-method="slideDown"
                                       href="javascript:void(0)" role="button">Сохранить</a>
                                </div>
                            </div>
                            <!-- End Example Heading With Desc Bottom -->
                        </div>

                        <div class="col-xxl-6 col-lg-6 col-xs-12">
                            <!-- Example Heading With Tag -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Участники</h3>
                                </div>
                                <div class="panel-body" ng-controller="CourseListUsers">
                                    <div class="example table-responsive">
                                        <table class="table"  id="example">
                                            <thead>
                                            <tr>
                                                <th      data-align="center">#</th>
                                                <th     data-align="center">Имя</th>
                                                <th data-align="center">Роль</th>
                                            </tr>
                                            </thead>
                                            <tbody id="light-pagination">
                                            <tr ng-repeat="course in data.courses">
                                                <td><span class="flag-icon flag-icon-{{course.flag}}"></span></td>
                                                <td><a href="index.php?app=prof_player&id={{course.id}}">{{course.name}}</a></td>
                                                <td>{{course.role}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End Example Heading   With Tag -->
                        </div>
                    </div>
                </div>
                <!-- End Panel -->
            </div>
        </div>
    </div>
</div>
<!-- End Page -->
<!-- Footer -->
<footer class="site-footer">
    <? require 'public/module/footer.php'?>
</footer>
<!-- Core  -->
<script src="global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
<script src="global/vendor/jquery/jquery.js"></script>
<script src="global/vendor/tether/tether.js"></script>
<script src="global/vendor/bootstrap/bootstrap.js"></script>
<script src="global/vendor/animsition/animsition.js"></script>
<script src="global/vendor/mousewheel/jquery.mousewheel.js"></script>
<script src="global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
<script src="global/vendor/asscrollable/jquery-asScrollable.js"></script>
<script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
<script src="global/vendor/waves/waves.js"></script>
<!-- Plugins -->
<script src="global/vendor/switchery/switchery.min.js"></script>
<script src="global/vendor/intro-js/intro.js"></script>
<script src="global/vendor/screenfull/screenfull.js"></script>
<script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
<script src="global/vendor/flot/jquery.flot.js"></script>
<script src="global/vendor/flot/jquery.flot.resize.js"></script>
<script src="global/vendor/flot/jquery.flot.time.js"></script>
<script src="global/vendor/flot/jquery.flot.stack.js"></script>
<script src="global/vendor/flot/jquery.flot.pie.js"></script>
<script src="global/vendor/flot/jquery.flot.selection.js"></script>

<script src="global/vendor/switchery/switchery.min.js"></script>
<script src="global/vendor/intro-js/intro.js"></script>
<script src="global/vendor/screenfull/screenfull.js"></script>
<script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
<script src="global/vendor/toastr/toastr.js"></script>
<!-- Scripts -->
<script src="global/js/State.js"></script>
<script src="global/js/Component.js"></script>
<script src="global/js/Plugin.js"></script>
<script src="global/js/Base.js"></script>
<script src="global/js/Config.js"></script>
<script src="assets/js/Section/Menubar.js"></script>
<script src="assets/js/Section/GridMenu.js"></script>
<script src="assets/js/Section/Sidebar.js"></script>
<script src="assets/js/Section/PageAside.js"></script>
<script src="assets/js/Plugin/menu.js"></script>
<script src="global/js/config/colors.js"></script>
<script src="assets/js/config/tour.js"></script>
<script>
    Config.set('assets', 'assets');
</script>
<!-- Page -->
<script src="assets/js/Site.js"></script>
<script src="global/js/Plugin/asscrollable.js"></script>
<script src="global/js/Plugin/slidepanel.js"></script>
<script src="global/js/Plugin/switchery.js"></script>
<script src="global/js/Plugin/responsive-tabs.js"></script>
<script src="global/js/Plugin/tabs.js"></script>

<script src="global/js/Plugin/alertify.js"></script>
<script src="global/js/Plugin/notie-js.js"></script>
<script src="global/js/Plugin/toastr.js"></script>
<script>
    (function(document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);
    var team='<?=$_GET['id'];?>';
    $(document).on('click','#button_about',function(e){
        e.preventDefault();
        //var id = $(this).data('id') || 0;
        contru=document.getElementById("contru").value;
        rules=document.getElementById("rules").value;
        textareaDefault=document.getElementById("textareaDefault").value;
        $("#out").load("public/scr/team_uset_id.php",{contru:contru,rules:rules,textareaDefault:textareaDefault,team:team});
        // работа с именем
        console.log(contru,rules,textareaDefault);
    });

    $(document).on('click','#code_gen',function(e){
        e.preventDefault();

        $("#out").load("public/scr/team_code_gen.php",{team:team});
        // работа с именем
        /* $('#team_add').removeClass('modal fade modal-fade-in-scale-up in').addClass(' modal fade modal-fade-in-scale-up');*/
    });
</script>
</body>
</html>

