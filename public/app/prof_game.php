<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <title>DataTables | Remark Admin Template</title>
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="global/css/bootstrap.min.css">
    <link rel="stylesheet" href="global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="assets/css/site.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="global/vendor/waves/waves.css">
    <link rel="stylesheet" href="global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
    <link rel="stylesheet" href="global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
    <link rel="stylesheet" href="global/vendor/datatables-responsive/dataTables.responsive.css">
    <link rel="stylesheet" href="assets/examples/css/tables/datatable.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="global/fonts/font-awesome/font-awesome.css">
    <link rel="stylesheet" href="global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <!--[if lt IE 9]>
    <script src="global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="global/vendor/media-match/media.match.min.js"></script>
    <script src="global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
    </script>
</head>
<body class="animsition">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
            <i class="icon md-more" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <img class="navbar-brand-logo" src="assets/images/logo.png" title="Remark">
            <span class="navbar-brand-text hidden-xs-down"> Remark</span>
        </div>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search"
                data-toggle="collapse">
            <span class="sr-only">Toggle Search</span>
            <i class="icon md-search" aria-hidden="true"></i>
        </button>
    </div>
    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar -->
            <ul class="nav navbar-toolbar">
                <li class="nav-item hidden-float" id="toggleMenubar">
                    <a class="nav-link" data-toggle="menubar" href="#" role="button">
                        <i class="icon hamburger hamburger-arrow-left">
                            <span class="sr-only">Toggle menubar</span>
                            <span class="hamburger-bar"></span>
                        </i>
                    </a>
                </li>
                <li class="nav-item hidden-sm-down" id="toggleFullscreen">
                    <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                        <span class="sr-only">Toggle fullscreen</span>
                    </a>
                </li>
                <li class="nav-item hidden-float">
                    <a class="nav-link icon md-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
                       role="button">
                        <span class="sr-only">Toggle Search</span>
                    </a>
                </li>
                <li class="nav-item dropdown dropdown-fw dropdown-mega">
                    <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false" data-animation="fade"
                       role="button">Mega <i class="icon md-chevron-down" aria-hidden="true"></i></a>
                    <div class="dropdown-menu" role="menu">
                        <div class="mega-content">
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <h5>UI Kit</h5>
                                    <ul class="blocks-2">
                                        <li class="mega-menu m-0">
                                            <ul class="list-icons">
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../advanced/animation.html">Animation</a>
                                                </li>
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../uikit/buttons.html">Buttons</a>
                                                </li>
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../uikit/colors.html">Colors</a>
                                                </li>
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../uikit/dropdowns.html">Dropdowns</a>
                                                </li>
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../uikit/icons.html">Icons</a>
                                                </li>
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../advanced/lightbox.html">Lightbox</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu m-0">
                                            <ul class="list-icons">
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../uikit/modals.html">Modals</a>
                                                </li>
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../uikit/panel-structure.html">Panels</a>
                                                </li>
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../structure/overlay.html">Overlay</a>
                                                </li>
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../uikit/tooltip-popover.html ">Tooltips</a>
                                                </li>
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../advanced/scrollable.html">Scrollable</a>
                                                </li>
                                                <li><i class="md-chevron-right" aria-hidden="true"></i>
                                                    <a
                                                        href="../uikit/typography.html">Typography</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <h5>Media
                                        <span class="tag tag-pill tag-success">4</span>
                                    </h5>
                                    <ul class="blocks-3">
                                        <li>
                                            <a class="thumbnail m-0" href="javascript:void(0)">
                                                <img class="w-full" src="global/photos/placeholder.png" alt="..." />
                                            </a>
                                        </li>
                                        <li>
                                            <a class="thumbnail m-0" href="javascript:void(0)">
                                                <img class="w-full" src="global/photos/placeholder.png" alt="..." />
                                            </a>
                                        </li>
                                        <li>
                                            <a class="thumbnail m-0" href="javascript:void(0)">
                                                <img class="w-full" src="global/photos/placeholder.png" alt="..." />
                                            </a>
                                        </li>
                                        <li>
                                            <a class="thumbnail m-0" href="javascript:void(0)">
                                                <img class="w-full" src="global/photos/placeholder.png" alt="..." />
                                            </a>
                                        </li>
                                        <li>
                                            <a class="thumbnail m-0" href="javascript:void(0)">
                                                <img class="w-full" src="global/photos/placeholder.png" alt="..." />
                                            </a>
                                        </li>
                                        <li>
                                            <a class="thumbnail m-0" href="javascript:void(0)">
                                                <img class="w-full" src="global/photos/placeholder.png" alt="..." />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <h5 class="m-b-0">Accordion</h5>
                                    <!-- Accordion -->
                                    <div class="panel-group panel-group-simple" id="siteMegaAccordion" aria-multiselectable="true"
                                         role="tablist">
                                        <div class="panel">
                                            <div class="panel-heading" id="siteMegaAccordionHeadingOne" role="tab">
                                                <a class="panel-title" data-toggle="collapse" href="#siteMegaCollapseOne" data-parent="#siteMegaAccordion"
                                                   aria-expanded="false" aria-controls="siteMegaCollapseOne">
                                                    Collapsible Group Item #1
                                                </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="siteMegaCollapseOne" aria-labelledby="siteMegaAccordionHeadingOne"
                                                 role="tabpanel">
                                                <div class="panel-body">
                                                    De moveat laudatur vestra parum doloribus labitur sentire partes, eripuit praesenti
                                                    congressus ostendit alienae, voluptati ornateque accusamus
                                                    clamat reperietur convicia albucius.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading" id="siteMegaAccordionHeadingTwo" role="tab">
                                                <a class="panel-title collapsed" data-toggle="collapse" href="#siteMegaCollapseTwo"
                                                   data-parent="#siteMegaAccordion" aria-expanded="false"
                                                   aria-controls="siteMegaCollapseTwo">
                                                    Collapsible Group Item #2
                                                </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="siteMegaCollapseTwo" aria-labelledby="siteMegaAccordionHeadingTwo"
                                                 role="tabpanel">
                                                <div class="panel-body">
                                                    Praestabiliorem. Pellat excruciant legantur ullum leniter vacare foris voluptate
                                                    loco ignavi, credo videretur multoque choro fatemur
                                                    mortis animus adoptionem, bello statuat expediunt naturales.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading" id="siteMegaAccordionHeadingThree" role="tab">
                                                <a class="panel-title collapsed" data-toggle="collapse" href="#siteMegaCollapseThree"
                                                   data-parent="#siteMegaAccordion" aria-expanded="false"
                                                   aria-controls="siteMegaCollapseThree">
                                                    Collapsible Group Item #3
                                                </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="siteMegaCollapseThree" aria-labelledby="siteMegaAccordionHeadingThree"
                                                 role="tabpanel">
                                                <div class="panel-body">
                                                    Horum, antiquitate perciperet d conspectum locus obruamus animumque perspici probabis
                                                    suscipere. Desiderat magnum, contenta poena desiderant
                                                    concederetur menandri damna disputandum corporum.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Accordion -->
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <!-- End Navbar Toolbar -->
            <!-- Navbar Toolbar Right -->
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" data-animation="scale-up"
                       aria-expanded="false" role="button">
                        <span class="flag-icon flag-icon-ru"></span>
                    </a>

                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                       data-animation="scale-up" role="button">
              <span class="avatar avatar-online">
                <img src="global/portraits/5.jpg" alt="...">
                <i></i>
              </span>
                    </a>
                    <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Profile</a>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-card" aria-hidden="true"></i> Billing</a>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-settings" aria-hidden="true"></i> Settings</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Notifications"
                       aria-expanded="false" data-animation="scale-up" role="button">
                        <i class="icon md-notifications" aria-hidden="true"></i>
                        <span class="tag tag-pill tag-danger up">5</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                        <div class="dropdown-menu-header">
                            <h5>NOTIFICATIONS</h5>
                            <span class="tag tag-round tag-danger">New 5</span>
                        </div>
                        <div class="list-group">
                            <div data-role="container">
                                <div data-role="content">
                                    <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left p-r-10">
                                                <i class="icon md-receipt bg-red-600 white icon-circle" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">A new order has been placed</h6>
                                                <time class="media-meta" datetime="2017-06-12T20:50:48+08:00">5 hours ago</time>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left p-r-10">
                                                <i class="icon md-account bg-green-600 white icon-circle" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Completed the task</h6>
                                                <time class="media-meta" datetime="2017-06-11T18:29:20+08:00">2 days ago</time>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left p-r-10">
                                                <i class="icon md-settings bg-red-600 white icon-circle" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Settings updated</h6>
                                                <time class="media-meta" datetime="2017-06-11T14:05:00+08:00">2 days ago</time>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left p-r-10">
                                                <i class="icon md-calendar bg-blue-600 white icon-circle" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Event started</h6>
                                                <time class="media-meta" datetime="2017-06-10T13:50:18+08:00">3 days ago</time>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left p-r-10">
                                                <i class="icon md-comment bg-orange-600 white icon-circle" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Message received</h6>
                                                <time class="media-meta" datetime="2017-06-10T12:34:48+08:00">3 days ago</time>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown-menu-footer">
                            <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                                <i class="icon md-settings" aria-hidden="true"></i>
                            </a>
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                All notifications
                            </a>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Messages"
                       aria-expanded="false" data-animation="scale-up" role="button">
                        <i class="icon md-email" aria-hidden="true"></i>
                        <span class="tag tag-pill tag-info up">3</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                        <div class="dropdown-menu-header" role="presentation">
                            <h5>MESSAGES</h5>
                            <span class="tag tag-round tag-info">New 3</span>
                        </div>
                        <div class="list-group" role="presentation">
                            <div data-role="container">
                                <div data-role="content">
                                    <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left p-r-10">
                          <span class="avatar avatar-sm avatar-online">
                            <img src="global/portraits/2.jpg" alt="..." />
                            <i></i>
                          </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Mary Adams</h6>
                                                <div class="media-meta">
                                                    <time datetime="2017-06-17T20:22:05+08:00">30 minutes ago</time>
                                                </div>
                                                <div class="media-detail">Anyways, i would like just do it</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left p-r-10">
                          <span class="avatar avatar-sm avatar-off">
                            <img src="global/portraits/3.jpg" alt="..." />
                            <i></i>
                          </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Caleb Richards</h6>
                                                <div class="media-meta">
                                                    <time datetime="2017-06-17T12:30:30+08:00">12 hours ago</time>
                                                </div>
                                                <div class="media-detail">I checheck the document. But there seems</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left p-r-10">
                          <span class="avatar avatar-sm avatar-busy">
                            <img src="global/portraits/4.jpg" alt="..." />
                            <i></i>
                          </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">June Lane</h6>
                                                <div class="media-meta">
                                                    <time datetime="2017-06-16T18:38:40+08:00">2 days ago</time>
                                                </div>
                                                <div class="media-detail">Lorem ipsum Id consectetur et minim</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left p-r-10">
                          <span class="avatar avatar-sm avatar-away">
                            <img src="global/portraits/5.jpg" alt="..." />
                            <i></i>
                          </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Edward Fletcher</h6>
                                                <div class="media-meta">
                                                    <time datetime="2017-06-15T20:34:48+08:00">3 days ago</time>
                                                </div>
                                                <div class="media-detail">Dolor et irure cupidatat commodo nostrud nostrud.</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown-menu-footer" role="presentation">
                            <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                                <i class="icon md-settings" aria-hidden="true"></i>
                            </a>
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                See all messages
                            </a>
                        </div>
                    </div>
                </li>
                <li class="nav-item" id="toggleChat">
                    <a class="nav-link" data-toggle="site-sidebar" href="javascript:void(0)" title="Chat"
                       data-url="../site-sidebar.tpl">
                        <i class="icon md-comment" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
            <!-- End Navbar Toolbar Right -->
        </div>
        <!-- End Navbar Collapse -->
        <!-- Site Navbar Seach -->
        <div class="collapse navbar-search-overlap" id="site-navbar-search">
            <form role="search">
                <div class="form-group">
                    <div class="input-search">
                        <i class="input-search-icon md-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="site-search" placeholder="Search...">
                        <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
                                data-toggle="collapse" aria-label="Close"></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- End Site Navbar Seach -->
    </div>
</nav>
<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu" data-plugin="menu">
                    <li class="site-menu-category">Основные</li>
                    <li class="site-menu-item ">
                        <a class="animsition-link" href="index.html">
                            <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                            <span class="site-menu-title">Панель Управления</span>
                        </a>
                    </li>
                    <li class="site-menu-item active">
                        <a class="animsition-link" href="list_all.html">
                            <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                            <span class="site-menu-title">Список</span>
                        </a>
                    </li>
                    <li class="site-menu-item ">
                        <a class="animsition-link" href="index.html">
                            <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                            <span class="site-menu-title">Календарь</span>
                        </a>
                    </li>
                    <li class="site-menu-item ">
                        <a class="animsition-link" href="chat.html">
                            <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                            <span class="site-menu-title">Чат</span>
                        </a>
                    </li>
                </ul>
                <div class="site-menubar-section">
                    <h5>
                        Milestone
                        <span class="pull-xs-right">30%</span>
                    </h5>
                    <div class="progress progress-xs">
                        <div class="progress-bar active" style="width: 30%;" role="progressbar"></div>
                    </div>
                    <h5>
                        Release
                        <span class="pull-xs-right">60%</span>
                    </h5>
                    <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-warning" style="width: 60%;" role="progressbar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-menubar-footer">
        <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
           data-original-title="Settings">
            <span class="icon md-settings" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
            <span class="icon md-eye-off" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
            <span class="icon md-power" aria-hidden="true"></span>
        </a>
    </div>
</div>
<div class="site-gridmenu">
    <div>
        <div>
            <ul>
                <li>
                    <a href="../apps/mailbox/mailbox.html">
                        <i class="icon md-email"></i>
                        <span>Mailbox</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/calendar/calendar.html">
                        <i class="icon md-calendar"></i>
                        <span>Calendar</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/contacts/contacts.html">
                        <i class="icon md-account"></i>
                        <span>Contacts</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/media/overview.html">
                        <i class="icon md-videocam"></i>
                        <span>Media</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/documents/categories.html">
                        <i class="icon md-receipt"></i>
                        <span>Documents</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/projects/projects.html">
                        <i class="icon md-image"></i>
                        <span>Project</span>
                    </a>
                </li>
                <li>
                    <a href="../apps/forum/forum.html">
                        <i class="icon md-comments"></i>
                        <span>Forum</span>
                    </a>
                </li>
                <li>
                    <a href="../index.html">
                        <i class="icon md-view-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Page -->
<div class="page">
    <div class="page-header">
        <h1 class="page-title">Invoice</h1>
    </div>
    <div class="page-content">
        <!-- Panel -->
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="col-xs-12 col-lg-6">
                    <div class="col-xs-12 col-lg-3">
                        <h4>
                            <img class="m-r-10" src="assets//images/logo-blue.png" alt="..."><?=$_GET[name];?></h4>
                        <address>
                            Описание команды которая что то из себя представляет
                        </address>
                    </div>
                    <div class="col-xs-12 col-lg-3 text-xs-right">
                        <h4>Основной состав</h4>
                        <p>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>

                        </p>

                    </div>
                    <div class="col-xs-12 col-lg-3 text-xs-right" >
                        <h4>Второй состав состав</h4>
                        <p>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>

                        </p>

                    </div>
                    <div class="col-xs-12 col-lg-3 text-xs-right">
                        <h4>Админестрация </h4>
                        <p>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>
                            Капитан <a class="font-size-20" href="javascript:void(0)">Кукуреков</a>
                            <br>

                        </p>

                    </div>
                    <div class="col-xs-12 col-lg-12">
                        <div class="example example-buttons">
                            <button type="button" class="btn btn-labeled social-facebook waves-effect">
                                <span class="btn-label"><i class="icon bd-facebook" aria-hidden="true"></i></span>Назначить встречу</button>
                            <button type="button" class="btn btn-labeled social-twitter waves-effect">
                                <span class="btn-label"><i class="icon bd-twitter" aria-hidden="true"></i></span>Подписатся</button>

                            <button type="button" class="btn btn-labeled social-linkedin waves-effect">
                                <span class="btn-label"><i class="icon bd-linkedin" aria-hidden="true"></i></span>Одобрить</button>
                            <button type="button" class="btn btn-labeled social-flickr waves-effect">
                                <span class="btn-label"><i class="icon bd-flickr" aria-hidden="true"></i></span>Добавить в круг общения</button>
                            <button type="button" class="btn btn-labeled social-tumblr waves-effect">
                                <span class="btn-label"><i class="icon bd-tumblr" aria-hidden="true"></i></span>Пожаловатся</button>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6">

                    <table class="table table-hover text-xs-right" id="example">
                        <thead>
                        <tr>
                            <th class="text-xs-center">#</th>
                            <th class="text-xs-left">Команда 1</th>
                            <th class="text-xs-center">Счет</th>
                            <th class="text-xs-right">Команда 2</th>
                            <th class="text-xs-center">Карта</th>
                            <th class="text-xs-center">Правила</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        <tr>
                            <td class="text-xs-center">
                                1
                            </td>
                            <td class="text-xs-left">
                                Курицы
                            </td>
                            <td class="text-xs-center">
                                10-5
                            </td>
                            <td class="text-xs-right">
                                Яблоко
                            </td>

                            <td>
                                ДЦ
                            </td>
                            <td>
                                Арена
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <!-- End Panel -->
    </div>
</div>
<!-- End Page -->
<!-- Footer -->
<footer class="site-footer">
    <div class="site-footer-legal">© 2017 <a href="https://vk.com/dimanoo">Diman0o</a></div>
    <div class="site-footer-right">
        При участии Клана <i class="red-600 icon md-favorite"></i>  <a href="#"> uNiteD*5tars</a>
    </div>
</footer>
<!-- Core  -->

<script src="global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
<script src="global/vendor/jquery/jquery.js"></script>
<script src="global/vendor/tether/tether.js"></script>
<script src="global/vendor/bootstrap/bootstrap.js"></script>
<script src="global/vendor/animsition/animsition.js"></script>
<script src="global/vendor/mousewheel/jquery.mousewheel.js"></script>
<script src="global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
<script src="global/vendor/asscrollable/jquery-asScrollable.js"></script>
<script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
<script src="global/vendor/waves/waves.js"></script>
<!-- Plugins -->

<script src="global/vendor/switchery/switchery.min.js"></script>
<script src="global/vendor/intro-js/intro.js"></script>
<script src="global/vendor/screenfull/screenfull.js"></script>
<script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
<script src="global/vendor/datatables/jquery.dataTables.js"></script>
<script src="global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
<script src="global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>
<script src="global/vendor/asrange/jquery-asRange.min.js"></script>
<script src="global/vendor/bootbox/bootbox.js"></script>
<!-- Scripts -->
<script src="global/js/State.js"></script>
<script src="global/js/Component.js"></script>
<script src="global/js/Plugin.js"></script>
<script src="global/js/Base.js"></script>
<script src="global/js/Config.js"></script>
<script src="assets/js/Section/Menubar.js"></script>
<script src="assets/js/Section/GridMenu.js"></script>
<script src="assets/js/Section/Sidebar.js"></script>
<script src="assets/js/Section/PageAside.js"></script>
<script src="assets/js/Plugin/menu.js"></script>
<script src="global/js/config/colors.js"></script>
<script src="assets/js/config/tour.js"></script>

<!-- Page -->
<script src="assets/js/Site.js"></script>
<script src="global/js/Plugin/asscrollable.js"></script>
<script src="global/js/Plugin/slidepanel.js"></script>
<script src="global/js/Plugin/switchery.js"></script>

<script>

    $(document).ready(function() {
        // Setup - add a text input to each footer cell
        $('.input-filter').each( function () {
            var title = $(this).text();
            $(this).html( '<input class="form-control input-sm" type="text" placeholder="'+title+'" />' );
        } );

        // DataTable


        var table = $('#example').DataTable( {
            initComplete: function () {
                this.api().columns('.select-filter').every( function () {
                    var column = this;
                    var select = $('<select  class="form-control input-sm"><option value="" ></option></select>')
                        .appendTo( $(column.header()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            },
            "columnDefs": [
                { "width": "5%", "targets": 0 },
                { "width": "30%", "targets": 1 },
                { "width": "5%", "targets": 2 },
                { "width": "30%", "targets": 3 },
                { "width": "5%", "targets": 4 },
                { "width": "5%", "targets": 5 }
            ],
            buttons: true,
            ordering: false,

            language: {
                search: "Поиск:",
                decimal:        "",
                emptyTable:     "Нет данных, доступных в таблице",
                info:           "Показывать  _START_ из _END_ всего _TOTAL_ Записей",
                infoEmpty:      "Показывать  0 из 0 всего 0 Записей",
                infoFiltered:   "(Отфильтрован из _MAX_ total Команд)",
                infoPostFix:    "",
                thousands:      ",",
                lengthMenu:     "Показывать _MENU_ Команд",
                loadingRecords: "Загрузка...",
                processing:     "Поцесс...",
                zeroRecords:    "No matching records found",
                paginate: {
                    first:      "Первая",
                    last:       "Последняя",
                    next:       "Следующая",
                    previous:   "Предыдущая"
                },
                aria: {
                    "sortAscending":  ": Активировать для сортировки столбцов по возрастанию",
                    "sortDescending": ": Активировать сортировку по убыванию"
                }
            }
        });

        // Apply the search
        table.columns('.input-filter').every( function () {
            var that = this;

            $( 'input', this.header() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );
    } );
</script>

<script src="assets/examples/js/uikit/icon.js"></script>
</body>
</html>



