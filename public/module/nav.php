<li class="nav-item dropdown">
    <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" data-animation="scale-up"
       aria-expanded="false" role="button">
        <span class="flag-icon flag-icon-ru"></span>
    </a>

</li>
<li class="nav-item dropdown">
    <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
       data-animation="scale-up" role="button">
              <span class="avatar avatar-online">
                <img src="global/portraits/5.jpg" alt="...">
                <i></i>
              </span>
    </a>
    <div class="dropdown-menu" role="menu">
        <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Профиль</a>

        <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-settings" aria-hidden="true"></i> Настройки</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="public/scr/exit.php" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Выход</a>
    </div>
</li>
<li class="nav-item dropdown">
    <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Уведомления"
       aria-expanded="false" data-animation="scale-up" role="button">
        <i class="icon md-notifications" aria-hidden="true"></i>
        <span id="notifications_col" class="tag tag-pill tag-danger up"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
        <div class="dropdown-menu-header">
            <h5>Уведомления</h5>
            <span id="notifications_col_ul" class="tag tag-round tag-danger"></span>
        </div>
        <div class="list-group">
            <div data-role="container">
                <div id="nav_on"></div>
                <div id="notifications_col_ul_content" data-role="content">
                    <h4 id="pusto" style="text-align: center"> Пусто</h4>
                </div>
            </div>
        </div>
        <div class="dropdown-menu-footer">
            <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                <i class="icon md-settings" aria-hidden="true"></i>
            </a>
            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                Все события
            </a>
        </div>
    </div>
</li>
<li class="nav-item dropdown">
    <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Сообщения"
       aria-expanded="false" data-animation="scale-up" role="button">
        <i class="icon md-email" aria-hidden="true"></i>
        <span class="tag tag-pill tag-info up"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
        <div class="dropdown-menu-header" role="presentation">
            <h5>Сообщения</h5>
            <span class="tag tag-round tag-info"></span>
        </div>
        <div class="list-group" role="presentation">
            <div data-role="container">
                <div data-role="content">
                    <h4 style="text-align: center">Новых нет</h4>
                </div>
            </div>
        </div>
        <div class="dropdown-menu-footer" role="presentation">
            <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                <i class="icon md-settings" aria-hidden="true"></i>
            </a>
            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                Все сообщения
            </a>
        </div>
    </div>
</li>
<li class="nav-item" id="toggleChat">
    <a class="nav-link" data-toggle="site-sidebar" href="javascript:void(0)" title="Chat"
       data-url="../site-sidebar.tpl">
        <i class="icon md-comment" aria-hidden="true"></i>
    </a>
</li>